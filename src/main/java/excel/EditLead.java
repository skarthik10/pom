package excel;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class EditLead {    
		public static Object[][] readEdit() throws IOException {
			XSSFWorkbook wb = new XSSFWorkbook("./data/EditLead.xlsx");
			XSSFSheet sheet = wb.getSheetAt(0);
			int rowcount = sheet.getLastRowNum();
			System.out.println(rowcount);
			int cellcount = sheet.getRow(0).getLastCellNum();
			System.out.println(cellcount);
			Object[][] data = new Object[rowcount][cellcount];
			for (int j = 1; j <= rowcount; j++) {
				XSSFRow row = sheet.getRow(j);
				for (int i=0;i<cellcount;i++) {
					XSSFCell cell = row.getCell(i);
					try {
						String value = cell.getStringCellValue();
						System.out.println(value);
						data[j-1][i] = value;
					} catch (NullPointerException e) {
						System.out.println("");
					}
					
					
				}
				
			}
			

			wb.close();
			return data;
			
		}

	}

