package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class CreateLead extends ProjectMethods {
	
	public CreateLead() {
		PageFactory.initElements(driver, this);
	}
	@FindBy(how = How.ID, using ="createLeadForm_companyName") WebElement elecname;
	@FindBy(how = How.ID, using ="createLeadForm_firstName") WebElement elefname;
	@FindBy(how = How.ID, using ="createLeadForm_lastName") WebElement elelname;
	@FindBy(how = How.NAME, using="submitButton") WebElement eleclick;
	
	public CreateLead enterCompanyName(String cname) {
		type(elecname, cname);
		return this;
	}
	public CreateLead enterFirstName(String fname) {
		type(elefname, fname);
		return this;
	}
	public CreateLead enterLastName(String lname) {
		type(elelname, lname);
		return this;
	}
	public ViewLead clickSubmitButton() {
		click(eleclick);
		return new ViewLead();
	}
}

