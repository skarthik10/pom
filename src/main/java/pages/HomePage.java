package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class HomePage extends ProjectMethods {
	
	public HomePage() {
		PageFactory.initElements(driver, this);
		
	}
	@FindBy(how = How.LINK_TEXT, using = "CRM/SFA") WebElement eleCRMSFA;
	
	public MyHome clickcrmsfa() {
		
		click(eleCRMSFA);
		return new MyHome();
	}
	
}
