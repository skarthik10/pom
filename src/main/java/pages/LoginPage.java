package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class LoginPage extends ProjectMethods{
	
	public LoginPage() {
		PageFactory.initElements(driver, this);
	}
	@FindBy(how = How.ID , using = "username") WebElement eleUserName;
	@FindBy(how = How.ID, using ="password") WebElement elepassword;
	@FindBy(how = How.CLASS_NAME, using ="decorativeSubmit") WebElement eleLogin;
	
	public LoginPage enterusername(String uname) {
		type(eleUserName, uname);
		return this;
	}
	public LoginPage enterpassword(String password) {
		type(elepassword, password);
		return this;
	}
	
	public HomePage clicklogin() {
		click(eleLogin);
		return new HomePage();
	
	}
}

