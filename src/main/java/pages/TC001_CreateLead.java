package pages;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class TC001_CreateLead extends ProjectMethods {
	
	@BeforeTest
	public void setData() {
		testCaseName = "Create Lead";
		testDesc = "Create a Lead";
		category = "smoke";
		author = "Karthikeyan";
		dataSheetName = "ReadExcel";	
		
	}
	@Test(dataProvider = "excel")
	public void createLead(String uname, String password, String cname, String fname, String lname) {
		LoginPage lp = new LoginPage();
		new LoginPage().enterusername(uname)
		.enterpassword(password)
		.clicklogin()
		.clickcrmsfa()
		.clickLeads()
		.clickcreateLead()
		.enterCompanyName(cname)
		.enterFirstName(fname)
		.enterLastName(lname)
		.clickSubmitButton();
	
		
	}
}
